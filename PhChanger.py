import pyperclip

phone_original = pyperclip.paste()

phone = ""

for character in phone_original:
    if character.isdigit():
        phone += character

pyperclip.copy(phone)